---
hide:
  - toc
author: à compléter
title: Préambule
---

# Préambule

**IMAGE**

Bienvenue sur notre plateforme dédiée à la cybersécurité ! 

Notre site offre des informations sur les **bonnes pratiques** en matière de sécurité informatique, des conseils pour **protéger ses données et sa vie privée**, ainsi que des astuces pour **détecter et contrer les menaces en ligne**.

Nous proposons également des **défis interactifs** pour mettre à l'épreuve vos compétences en cybersécurité, vous permettant de vous entraîner tout en vous amusant. En complément de ceux-ci, nous proposons une **sélection de plateformes** externes renommées, où vous pourrez aussi tester vos compétences dans des environnements variés. 

Nous présentons également une **gamme d'outils puissants, libres et actualisés** pour vous aider à résoudre les défis, qu'il s'agisse de cryptographie, d'analyse de logiciels malveillants ou de sécurisation des réseaux. Avec ces ressources à portée de main, vous serez prêt à relever tous les défis de cybersécurité qui se présenteront à vous.

Que vous soyez débutant, simple curieux ou d'un niveau avancé, **notre plateforme d'apprentissage est adaptée à tous** et renforcera vos connaissances et compétences en matière de cybersécurité.


Toutes les ressources présentées sur ce site sont partagées gratuitement sous licence <a href='https://creativecommons.org/licenses/by-sa/4.0/' target="_blank">CC BY-SA 4.0</a> afin de fournir des <span style='font-style:italic;'>Ressources Educatives Libres</span> (en abrégé, <span style='font-style:italic;'>REL</span>) comme <span style='font-style:italic;'>communs numériques</span>. Le but est de permettre à toutes et tous, quel que soit son niveau, de développer des compétences fortes en matière de protection de sa vie personnelle et, plus généralement, en matière de cybersécurité.

<br>

Les auteurs encouragent vivement le partage, la modification et la réutilisation des contenus afin que tout le monde puisse collaborer pour améliorer les ressources existantes, entraînant ainsi une synergie collective et une richesse de connaissances partagées. Les contenus peuvent être mis à jour pour refléter les avancées et les changements dans les domaines de connaissances afin de toujours garder des contenus pertinents et actuels.

<br>

Les auteurs soutiennent également une approche ouverte, collaborative et inclusive de l'Education, qui favorise la diffusion du savoir, l'innovation et l'équité dans l'accès à l'Education. L’exploitation gratuite et sans restrictions d'utilisation des 
_Ressources Educatives Libres_ sont rendues possible par les cinq R, piliers des REL :

- RETENIR : télécharger, dupliquer, conserver, le contenu autant de fois ou aussi longtemps que vous le voulez ;
- RÉUTILISER : utiliser le contenu de diverses manières selon vos propres fins ;
- RÉVISER : adapter, modifier ou traduire la ressource ;
- REMIXER : combiner la ressource (ou ses modifications) à une autre ;
- REDISTRIBUER : partager la ressource (ou ses modifications) avec d’autres.


Enfin, les auteurs soutiennent toutes les actions ayant pour objectif la mixité, notamment dans les Sciences, et plus spécifiquement ici dans l'Informatique. Ainsi, ils ont vivement souhaité mettre en avant les rôles des femmes dans l'Informatique en choisissant de présenter dans chaque partie un portrait de femme informaticienne.