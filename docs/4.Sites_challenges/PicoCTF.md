---
hide:
  - toc
author: à compléter
title: PicoCTF
---

# PicoCTF

![](../images_sites_challenges/Plateforme_PicoCTF.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Présentation générale de la plateforme"

    **PicoCTF** est une plateforme proposant dans l'ensemble des challenges assez simples, mais souvent très utiles pour s'initier à une nouvelle catégorie de challenges. Elle propose également une catégorie _General Skills_ qui est une bonne introduction à la logique du pentest.


    - **Type de challenges :** généraliste
    - **Niveau des challenges :** débutant
    - **Lien vers la plateforme :** <a href='https://picoctf.org/' target='_blank'>https://picoctf.org/</a>