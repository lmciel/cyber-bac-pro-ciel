---
hide:
  - toc
author: à compléter
title: Cryptohack
---

# Cryptohack

![](../images_sites_challenges/Plateforme_Cryptohack.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Présentation générale de la plateforme"

    **Cryptohack** est une excellente plateforme ne proposant que des challenges de cryptographie. Elle est idéale pour faire ses premiers pas dans cette catégorie grâce aux différents cours qui sont proposés pour aider les participants à progresser. Les thèmes abordés au travers des challenges sont extrêmement vastes : du chiffrement mathématiques basique au chiffrement quantique, en passant par les chiffrements symétriques et asymétriques, les courbes elliptiques et les fonctions de hachages.


    - **Type de challenges :** cryptographie
    - **Niveau des challenges :** bon niveau
    - **Lien vers la plateforme :** <a href='https://cryptohack.org/' target='_blank'>https://cryptohack.org/</a>