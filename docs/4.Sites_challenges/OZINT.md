---
hide:
  - toc
author: à compléter
title: OZINT
---

# OZINT

![](../images_sites_challenges/Plateforme_OZINT.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Présentation générale de la plateforme"

    **OZINT** est une plateforme entièrement dédiée à la pratique de l'OSINT. On y trouve de nombreux challenges de difficulté croissante pour progresser, des contenus pédagogiques sur des outils ou des méthodologies spécifiques à l'OSINT, ainsi que des outils pour la pratique, des informations et des conseils. On peut également créer des équipes pour partager et ne pas évoluer seul.


    - **Type de challenges :** OSINT
    - **Niveau des challenges :** bon niveau
    - **Lien vers la plateforme :** <a href='https://ozint.eu/' target='_blank'>https://ozint.eu/</a>