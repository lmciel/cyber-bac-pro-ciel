---
hide:
  - toc
author: à compléter
title: Programmation Python/Scratch
---

# Programmation Python/Scratch

![](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/true.png){: .center }


!!! tip "Jean Bartik"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/jean_bartik.png){: .center }

    JUILLET 1943 — 1946
    Les programmeurs ENIAC comprenaient un certain nombre de femmes : <a href="https://fr.wikipedia.org/wiki/Jean_Bartik" target='_blank'>Jean Bartik</a>, Marlyn Wescoff Meltzer, Ruth Lichterman Teitelbaum, Kay McNulty Mauchly Antonelli, Frances Spence et Frances Elizabeth « Betty » Holberton. Malgré leur travail révolutionnaire, l'armée n'a jamais divulgué les noms des femmes qui ont travaillé au sein de l'ENIAC, et elles ont été largement oubliées jusqu'à ce que Kathy Kleiman découvre leur histoire en 1985.

    <a href='https://youtu.be/aPweFhhXFvY?si=ePwrDlJJFaEcIUYq'>Jean Bartik and the ENIAC Women</a>




!!! abstract "Description générale de la catégorie"

    Dans cette catégorie, on propose divers challenges dont la résolution conduit à la lecture ou à l'écriture de programmes Python ou Scratch.

    



<hr style="height:5px;color:red;background-color:red;">


!!! note "Age glaciaire"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/AgeGlace/age_glace.png){: .center }
    
    Sur la banquise, vivait un pingouin curieux nommé Pipou. Un jour, il découvrit un parchemin mystérieux indiquant l'emplacement d'un trésor. Aidez le à le trouver !

    **Difficulté :** ⭐ 3 flags

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/AgeGlace/age_glace/){ .md-button target="_blank" rel="noopener" }


!!! note "Art Expo (d'après <a href='https://www.101computing.net/'>101computing.net</a>)"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/ArtExpo/art.png){: .center }
    
    La grande exposition Art Expo va bientôt être inaugurée ! Vous devez aidez le galeriste à accéder aux douze tableaux les plus célèbres du monde avant cette événement international !

    **Difficulté :** ⭐⭐ 12 flags

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/ArtExpo/art_expo/){ .md-button target="_blank" rel="noopener" }

!!! note "Blackbeard’s Treasure Map (d'après <a href='https://www.101computing.net/'>101computing.net</a>)"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/BlackbeardsTreasureMap/pirate-flag2.png){: .center }
    
    Différents fragments de parchemins viennent d'être découverts. Il semble qu'il indique l'emplacement du trésor du célèbre pirate Barbe-Noire. Saurez-vous faire preuve de sagacité et de perspicacité pour les déchiffer ?

    **Difficulté :** ⭐ 1 flag

    !!! warning "Challenge en anglais"
    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/BlackbeardsTreasureMap/anglais1.png)

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/BlackbeardsTreasureMap/BlackbeardsTreasureMap/){ .md-button target="_blank" rel="noopener" }
