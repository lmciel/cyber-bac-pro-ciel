---
hide:
  - toc
author: à compléter
title: Liste des challenges
---

En cliquant sur les entêtes, vous pouvez trier selon la catégorie, le titre du challenge, le nombre de flags ou la difficulté.

<center>
    <table>
        <thead>
            <tr>
                <th>Catégorie &#8645;</th>
                <th>Titre &#8645;</th>
                <th>Nombre de flags &#8645;</th>
				<th>Difficulté &#8645;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Algorithmique</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/BlackbeardsHiddenTreasures/BlackbeardsHiddenTreasures/" target='_blank'>Blackbeard’s Hidden Treasures</a></td>
                <td>12</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>Algorithmique</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/Cadenas/coffre_cadenas/" target='_blank'>Cadenas</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>Algorithmique</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/CadenasAlea/cadenas/" target='_blank'>Cadenas dynamiques</a></td>
                <td>3</td>
				<td>⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/AttaqueMDP/attaque_mdp/" target='_blank'>Attaque de mots de passe</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/CryptageRenforc%C3%A9/cryptage_renforce/" target='_blank'>Cryptage renforcé</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/DangerImmediat/danger_immediat/" target='_blank'>Danger immédiat</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/DossierProt%C3%A9g%C3%A9/dossier_protege/" target='_blank'>Dossier protégé</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Cryptographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/crypto/chall7/" target='_blank'>Grille tournante</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/OTomatesLight/o_tomates_light/" target='_blank'>O Tomates (version light)</a></td>
                <td>4</td>
				<td>⭐</td>
            </tr>
            <tr>
                <td>Cryptographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/OTomates/o_tomates/" target='_blank'>O Tomates (version épicée)</a></td>
                <td>4</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/A_velo/a_velo/" target='_blank'>A vélo</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Elementaire/elementaire_SH/" target='_blank'>Elémentaire... Vous avez dit élémentaire ?</a></td>
                <td>6</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Espionnage/espionnage/" target='_blank'>Espionnage</a></td>
                <td>2</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/FlanerieInsulaire/flanerie_insulaire/" target='_blank'>Flânerie insulaire</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/OperationOverlord/operation_overlord/" target='_blank'>Opération Overlord</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Qsales/qsales/" target='_blank'>Q Salés</a></td>
                <td>3</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/QuelleVue/quelle_vue/" target='_blank'>Quelle vue !</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
             <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Randonnee/randonnee/" target='_blank'>Randonnée</a></td>
                <td>11</td>
				<td>⭐⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Remparts/remparts/" target='_blank'>Remparts</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/GitSecrets/gitsecrets/" target='_blank'>Secrets de Forge</a></td>
                <td>3</td>
				<td>⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/TresorDisparu/tresor_disparu/" target='_blank'>Trésor disparu</a></td>
                <td>6</td>
				<td>⭐⭐⭐</td>
            </tr>
            <tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/ADN/adn/" target='_blank'>Un brin peut en cacher un autre</a></td>
                <td>4</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>OSINT</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/VeniVidiVicix6/venividivicix6/" target='_blank'>(veni, vidi, vici)x6</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Programmation Python/Scratch</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/AgeGlace/age_glace/" target='_blank'>Age glaciaire</a></td>
                <td>3</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Programmation Python/Scratch</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/ArtExpo/art_expo/" target='_blank'>Art Expo</a></td>
                <td>12</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Programmation Python/Scratch</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/BlackbeardsTreasureMap/BlackbeardsTreasureMap/" target='_blank'>Blackbeard's Treasure Map</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-reseaux/3.Challenges/ressources_challenges_reseaux/BasLesMasques/bas_les_masques/" target='_blank'>Bas les masques !</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-reseaux/3.Challenges/ressources_challenges_reseaux/ReseauLocal/reseau_local/" target='_blank'>Réseau local</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="https://www.challenges-kids.fr/categories/network/chall3/" target='_blank'>TELNET</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Réseaux</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-reseaux/3.Challenges/ressources_challenges_reseaux/TrameEthernet/trame_ethernet/" target='_blank'>Trame Ethernet</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/AsciiArt/ascii_art/" target='_blank'>ASCII Art</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/stegano/chall5/" target='_blank'>Casse-tête chinois</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
            <tr>
                <td>Stéganographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Gif/gif/" target='_blank'>Gif</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ImageMaBelleImage/image_ma_belle_image/" target='_blank'>Image, ma belle image</a></td>
                <td>3</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ImageCorrompue/image_corrompue/" target='_blank'>Image corrompue</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/culture/chall2/" target='_blank'>QR Code cassé</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://www.challenges-kids.fr/categories/culture/chall5/" target='_blank'>QR Code gagnant</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Stéganographie</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Spectrale/spectrale/" target='_blank'>Spectrale</a></td>
                <td>2</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N1/capture_the_fraise_N1/" target='_blank'>Capture The Fraise I</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N2/capture_the_fraise_N2/" target='_blank'>Capture The Fraise II</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N3/capture_the_fraise_N3/" target='_blank'>Capture The Fraise III</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N4/capture_the_fraise_N4/" target='_blank'>Capture The Fraise IV</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href="https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/ConnexionAdministrateur/connexion_administrateur/" target='_blank'>Connexion administrateur</a></td>
                <td>5</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href='https://www.challenges-kids.fr/categories/culture/chall3/' target='_blank'>Cookies or not cookies</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - client</td>
                <td><a href='https://www.root-me.org/fr/Challenges/Web-Client/HTML-boutons-desactives' target='_blank'>HTML - boutons désactivés</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
            <tr>
                <td>Web - client</td>
                <td><a href='https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/Motifs/motifs/' target='_blank'>Motifs</a></td>
                <td>5</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.challenges-kids.fr/categories/web/chall3/' target='_blank'>Répertoires</a></td>
                <td>1</td>
				<td>⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.root-me.org/fr/Challenges/Web-Serveur/SQL-injection-Authentification' target='_blank'>SQL injection - authentification</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.root-me.org/fr/Challenges/Web-Serveur/SQL-injection-String' target='_blank'>SQL injection - String</a></td>
                <td>1</td>
				<td>⭐⭐⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.challenges-kids.fr/categories/web/chall4/' target='_blank'>URL ?</a></td>
                <td>1</td>
				<td>⭐⭐</td>
            </tr>
			<tr>
                <td>Web - serveur</td>
                <td><a href='https://www.challenges-kids.fr/categories/web/chall8/' target='_blank'>Tetris</a></td>
                <td>1</td>
				<td>⭐⭐⭐⭐⭐⭐⭐⭐⭐⭐</td>
            </tr>
        </tbody>
    </table>
</center>

<script src='../tri_challenges.js'></script>