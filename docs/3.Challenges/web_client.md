---
hide:
  - toc
author: à compléter
title: Web - client
---

# Web - client

![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/delete.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Description générale de la catégorie"

    Le Web représente l'ensemble des liaisons entre les différents équipements électroniques et informatiques qui communiquent. Le versant &laquo;client&raquo; correspond à l'application qu'une personne utilisera pour naviguer sur le web, par exemple un navigateur web.

    Dans cette catégorie, on propose divers challenges permettant de manipuler quelques fonctionnalités d'un navigateur web, du plus simple comme l'affichage d'un code source, au plus compliqué avec l'utilisation de son inspecteur de code, en passant par la lecture de programme javascript.

    



<hr style="height:5px;color:red;background-color:red;">

!!! note "Capture The Fraise I"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N1/fraise_N1.png){: .center }
    
    Explorez un site web pour reconstituer le flag perdu

    N'oubliez pas : un site web n'est pas qu'une simple page html...

    **Difficulté :** ⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N1/capture_the_fraise_N1/){ .md-button target="_blank" rel="noopener" }


!!! note "Capture The Fraise II"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N2/fraise_N2.png){: .center }
    
    Devenez le champion de la récolte de fraises et explosez le score en un minimum de temps pour obtenir le flag !

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N2/capture_the_fraise_N2/){ .md-button target="_blank" rel="noopener" }


!!! note "Capture The Fraise III"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N3/fraise_N3.png){: .center }
    
    Bravo pour votre récolte de fraises ! Mais le flag ne vous sera dévoilé que si vous réussissez à effectuer la 
    bonne sauvegarde...

    **Difficulté :** ⭐⭐⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N3/capture_the_fraise_N3/){ .md-button target="_blank" rel="noopener" }


!!! note "Capture The Fraise IV"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N4/fraise_N4.png){: .center }
    
    Une dernière partie sur votre PC x86 avant la fin du monde informatique ?

    Si vous y arrivez, le flag vous sera dévoilé....

    **Difficulté :** ⭐⭐⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/CaptureTheFraise_N4/capture_the_fraise_N4/){ .md-button target="_blank" rel="noopener" }


!!! note "Connexion administrateur"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/ConnexionAdministrateur/image_connexion.png){: .center }
    
    Vous venez d'accéder à l'interface de connexion d'un administrateur système. Plus qu'un pas et vous obtiendrez son flag !
    
    Une initiation aux bonnes pratiques sur le développement Web.

    **Difficulté :** ⭐⭐ 5 flags

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/ConnexionAdministrateur/connexion_administrateur/){ .md-button target="_blank" rel="noopener" }


!!! note "Cookies or not cookies (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/Challenges_externes/cookies.png){: .center }
    
    Des cookies, des cookies et encore des cookies...
    
    Arriverez-vous à trouver le flag qui s'est caché parmi les options sur les cookies ?

    **Difficulté :** ⭐ 1 flag

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/culture/chall3/){ .md-button target="_blank" rel="noopener" }


!!! note "HTML - boutons désactivés (challenge issu de la plateforme <a href='https://www.root-me.org/' target='_blank'>Root Me</a>)"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/Challenges_externes/HTML_boutons_desactives.png){: .center }
    
    Un formulaire inutilisable ? A vous de contourner la situation pour récupérer le flag !

    **Difficulté :** ⭐ 1 flag

    <a href='https://www.root-me.org/' target='_blank'>Root Me</a> est une plateforme de CTF française très connue et très qualitative qui offre de nombreux challenges dans diverses catégories, ainsi que de nombreuses ressources.

    [_Accéder au challenge_](https://www.root-me.org/fr/Challenges/Web-Client/HTML-boutons-desactives){ .md-button target="_blank" rel="noopener" }


!!! note "Motifs"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/Motifs/image_chall_motifs.png){: .center }
    
    Cinq connexions à réaliser... Cinq énigmes... Cinq flags...

    Y parviendrez-vous ?

    **Difficulté :** ⭐ 5 flags

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/Motifs/motifs/){ .md-button target="_blank" rel="noopener" }