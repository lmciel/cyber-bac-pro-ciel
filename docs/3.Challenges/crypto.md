---
hide:
  - toc
author: à compléter
title: Cryptographie
---

# Cryptographie

![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/sha1.png){: .center }


!!! tip "Sara Sellos"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/sara2.png){: .center }

    <a href="https://fr.linkedin.com/in/sara-sellos-16638480" target='_blank'>Sara Sellos</a> est chef de département à la <a href='https://www.defense.gouv.fr/dga' target='_blank'>DGA (Direction 
    Générale de l'Armement)</a> et est ex-coordinatrice sectorielle défense à l'<a href='https://cyber.gouv.fr/' target='_blank'>ANSSI (Agence Nationale de la Sécurité des Systèmes d'Information)</a>

    Elle développe bénévolement, avec Saelyx, le site <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids</a>. 
    


!!! abstract "Description générale de la catégorie"

    La cryptographie est l'art de protéger une information en la rendant illisible à l'aide de différents procédés, comme, par exemple, des permutations de lettres ou de groupes de lettres, ou encore en utilisant des calculs mathématiques plus ou moins compliqués.

    Dans cette catégorie, on propose divers challenges ayant pour but de se familiariser avec diverses techniques, des plus simples, comme les chiffrements de César ou Vigenère, aux plus compliqués, comme la manipulation de fonctions de hachage, en passant par diverses méthodes classiques, comme les chiffrements en base 64 ou en base 32.

    



<hr style="height:5px;color:red;background-color:red;">

!!! note "Attaque de mots de passe"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/AttaqueMDP/image_attaque_mdp.png){: .center }
    
    Membre d'une équipe de cybersécurité, vous avez été mandaté par une entreprise pour tenter de pénétrer sur son intranet...

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/AttaqueMDP/attaque_mdp/){ .md-button target="_blank" rel="noopener" }

!!! note "Cryptage renforcé"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/CryptageRenforc%C3%A9/image_cryptage.png){: .center }
    
    Votre mission, si vous l'acceptez : pénétrez à l'intérieur d'un dossier hautement crypté et décodez le message chiffré qui s'y trouve.

    **Difficulté :** ⭐⭐⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/CryptageRenforc%C3%A9/cryptage_renforce/){ .md-button target="_blank" rel="noopener" }


!!! note "Danger immédiat"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/DangerImmediat/image_chall_danger_immediat.png){: .center }
    
    Membre de la DGSE, vous venez d'intercepter un message échangé entre les membres haut placés d'un groupe d'activistes.

    Votre mission : décodez le message avant qu'il ne soit trop tard...

    **Difficulté :** ⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/DangerImmediat/danger_immediat/){ .md-button target="_blank" rel="noopener" }


!!! note "Dossier protégé"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/DossierProt%C3%A9g%C3%A9/image_dossier.png){: .center }
    
    Notre agent a caché un message secret dans un dossier protégé par un mot de passe.

    Votre mission : pénétrez à l'intérieur de ce dossier et décodez le message avant qu'il ne soit trop tard...

    **Difficulté :** ⭐⭐ 1 flag

    [_Accéder au challenge_](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/DossierProt%C3%A9g%C3%A9/dossier_protege/){ .md-button target="_blank" rel="noopener" }



!!! note "Grille tournante (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/Challenges_externes/Handbuch_der_Kryptographe.png){: .center }
    
    Dix ans après son célèbre tour du monde en 80 jours, Phileas Fogg se lance dans une nouvelle expédition à la 
    poursuite d'un trésor légendaire : celui qui est caché dans la célèbre caverne d'Ali Baba. Après avoir réussi à 
    modifier le mot de passe magique qui protège l'entrée de la caverne, l'explorateur le chiffre avant de le 
    transmettre à ses compagnons de fortune...

    Arriverez-vous à le déchiffrer ?

    **Difficulté :** ⭐⭐ 1 flag

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/crypto/chall7/){ .md-button target="_blank" rel="noopener" }


!!! note "O Tomates"

    ![](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/OTomates/chall_o_tomate_intro.png){: .center }
    
    La suite du challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Qsales/qsales/' target='_blank'>Q Salés</a> dans lequel Roxane mène une enquête pour retrouver un tableau volé.

    Qui est-il réellement ? Parviendra-t-elle à le mettre hors d'état de nuire ?

    Votre mission : aidez Roxane pour venir à bout du voleur... ou pas...

    **Difficulté :** ⭐ 4 flags (version light) / ⭐⭐ 4 flags (version épicée)

    [_Accéder au challenge light_](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/OTomatesLight/o_tomates_light/){ .md-button target="_blank" rel="noopener" } [_Accéder au challenge épicé_](https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/OTomates/o_tomates/){ .md-button target="_blank" rel="noopener" }

    !!! warning "Avertissement"

        Il est recommandé d'avoir déjà traité le challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/Motifs/motifs/' target='_blank'>Motifs</a> puisque l'on retrouve ici certaines notions qui y ont été abordées
