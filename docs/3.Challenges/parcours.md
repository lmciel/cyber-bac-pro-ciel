---
hide:
  - toc
author: à compléter
title: Parcours
---

# Parcours

![](../images_parcours/parcours_challenges.png){: .center }


!!! tip "femme"

    à compléter


!!! abstract "Présentation générale"

    Dans cette page, on propose différents parcours de challenges permettant de travailler diverses compétences. Ces challenges peuvent être issus de différentes catégories.

    Les parcours sont regroupés en trois niveaux de difficultés : débutants, expérimentés et experts.

    Une description des compétences mises en avant est donnée pour chacun des parcours, ainsi que les liens directs aux challenges.




<hr style="height:5px;color:red;background-color:red;">

## Débutants

??? note "Débutant extrême"
    
    Un parcours de trois challenges faciles pour s'initier à la réalisation de challenges.
 
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/QuelleVue/quelle_vue/" target='_blank'>Quelle vue !</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/FlanerieInsulaire/flanerie_insulaire/" target='_blank'>Flânerie insulaire</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-cryptographie/3.Challenges/ressources_challenges_crypto/DangerImmediat/danger_immediat/" target='_blank'>Danger immédiat</a>

??? note "Melting pot"
    
    Un parcours de trois challenges pour manipuler la programmation Python avec Pipou, l'algorithmique avec des cadenas et la stéganographie avec un lapin.
 
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/AgeGlace/age_glace/" target='_blank'>Age glaciaire</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-algo/3.Challenges/ressources_challenges_algo/CadenasAlea/cadenas/" target='_blank'>Cadenas dynamiques</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/ImageMaBelleImage/image_ma_belle_image/" target='_blank'>Image, ma belle image</a>

## Expérimentés

??? note "Guillaume vs TheKalife"
    
    Suivez les aventures de Guillaume contre le célèbre hackeur TheKalife.

     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Remparts/remparts/" target='_blank'>Remparts</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/Gif/gif/" target='_blank'>Gif</a>


??? note "Programmation Python"
    
    Un parcours de trois challenges pour manipuler la programmation Python.

     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/BlackbeardsTreasureMap/BlackbeardsTreasureMap/" target='_blank'>Blackbeard's Treasure Map</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-steganographie/3.Challenges/ressources_challenges_stegano/AsciiArt/ascii_art/" target='_blank'>ASCII Art</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-programmation/3.Challenges/ressources_challenges_prog/ArtExpo/art_expo/" target='_blank'>Art Expo</a>


## Experts

??? note "100% OSINT"
    
    Un parcours de quatre challenges pour manipuler différents outils d'OSINT : un peu de littérature, deux challenges historiques et une randonnée dépaysante en montagne.

     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Elementaire/elementaire_SH/" target='_blank'>Elémentaire... Vous avez dit élémentaire ?</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/VeniVidiVicix6/venividivicix6/" target='_blank'>(veni, vidi, vici)x6</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/OperationOverlord/operation_overlord/" target='_blank'>Opération Overlord</a>
     - <a href="https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Randonnee/randonnee/" target='_blank'>Randonnée</a>