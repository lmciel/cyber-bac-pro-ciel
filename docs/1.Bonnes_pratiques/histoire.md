---
hide:
  - toc
author: à compléter
title: Une brève histoire

---

# Une brève histoire de la cybersécurité


**IMAGE**


!!! tip "femme"

    à compléter


!!! abstract "Une brève histoire de la cybersécurité"

    **La cybersécurité : un domaine aux racines très anciennes…**

    Bien que la cybersécurité soit souvent associée aux nouvelles technologies, elle a des racines anciennes dans la cryptographie. Depuis des siècles, les dirigeants ont utilisé des techniques de cryptage pour protéger leurs secrets, comme Jules César. Entre 1200 et 1650, l'État français a structuré ses fonctions essentielles, y compris la cryptographie, avec des figures comme Antoine Rossignol, cryptographe du roi sous Louis XIII et Louis XIV.

    **La sécurité des communications, au cœur des grands conflits mondiaux**

    La protection des communications a été cruciale lors des grands conflits mondiaux. Pendant la Première Guerre mondiale, Georges Painvain a décrypté le "Radiogramme de la Victoire", permettant une contre-offensive française décisive. Durant la Seconde Guerre mondiale, le travail d'Alan Turing et d'autres cryptanalystes, incluant les efforts franco-polonais menés par Gustave Bertrand et Marian Rejewski, a été central pour décrypter la machine Enigma allemande.

    **Avec l’essor de l’électronique, de l’informatique et d’internet, l’avènement de la sécurité des systèmes d’information (SSI)**

    Les années 1950 et 1960 voient des inventions clés comme le transistor et le microprocesseur, fondant l'informatique. Les décennies suivantes, avec l'essor des réseaux informatiques, établissent les bases de l'internet. Cela conduit à l'émergence de la sécurité des systèmes d'information, initialement dans les milieux gouvernemental et universitaire, puis se répandant avec la démocratisation d'internet pour couvrir de nombreux domaines techniques.
    
    **Le début du web : une (hyper-)connexion qui offre de fabuleuses opportunités et génère des menaces redoutables**

    Au début des années 1990, internet s'ouvre à l'usage commercial, accéléré dans les années 2000 par les services en ligne, l'internet mobile et le "web 2.0". Si cette évolution numérique crée des opportunités, elle engendre aussi des cybermenaces croissantes. En réponse, la cyberdéfense se développe, devenant un enjeu stratégique pour les États qui établissent des dispositifs techniques, politiques et réglementaires pour contrer les menaces d'espionnage et de sabotage, avec une importance croissante au niveau européen et international.


# 

