---
title: La cybersécurité expliquée à ma grand-mère
date: 2024-09-14
categories:
  - Ressources
  - Général
---


# La cybersécurité expliquée à ma grand-mère


![](images_posts/grandmere.jpg)

📢 #21 : Tractacus Logico Philosophicus

Pour comprendre l'importance des langages de programmation en matière de cybersécurité, et pourquoi il ne peut pas y avoir de sécurité absolue.

<!-- more -->

 <a href="https://podcast.ausha.co/la-cyber-securite-expliquee-a-ma-grand-mere/21-tractacus-logico-philosophicus
" target="_blank"> Podcast de 18min</a>

📢 #22 : Arnaques, Crimes et botanique


Pour comprendre pourquoi l'active directory est si important en cybersécurité ?

 <a href="https://podcast.ausha.co/la-cyber-securite-expliquee-a-ma-grand-mere/22-crime-arnaque-et-botanique
" target="_blank"> Podcast de 14min</a>

📢 #20 : American Gothic

Comment et pourquoi organiser sa gouvernance de cybersécurité en trois lignes de défense.

<a href="ttps://podcast.ausha.co/la-cyber-securite-expliquee-a-ma-grand-mere/20-american-gothic-ou-les-3-lignes-de-defense
" target="_blank"> Podcast de 09min</a>



 📢 <a href="https://podcast.ausha.co/la-cyber-securite-expliquee-a-ma-grand-mere
" target="_blank"> Tous les podcasts</a>
