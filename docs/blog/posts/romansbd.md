---
title: Romans et BD
date: 2024-09-14
categories:
  - Ressources
  - Général
---


# Romans et BD

📢 Des romans qui offrent une variété de perspectives sur les défis et les dangers liés à la cybersécurité, souvent mêlant suspense, action et réflexion éthique...

📢 Les BD de sensibilisation produites par la DSI  reflètent une situation concrète et implique un enseignant ou un acteur de l’Éducation Nationale...  et de quelques conseils pratiques et simples pour éviter les situations "dangereuses"...



<!-- more -->

Ces <a href="https://cybersecurite.forge.apps.education.fr/cyber/5.Ressources/romans/" target="_blank">romans</a>  offrent une variété de perspectives sur les défis et les dangers liés à la cybersécurité, souvent mêlant suspense, action et réflexion éthique.

- Soldat de la cyberguerre (2024)
- Cyberattaques : Les dessous d'une menace (2022)



Les <a href="https://cybersecurite.forge.apps.education.fr/cyber/5.Ressources/BD/" target="_blank"> BD de sensibilisation produites par la DSI</a>
Les documents proposés sont composés :

- d’une bande dessinée qui reflète une situation concrète et implique un enseignant ou un acteur de l’Éducation Nationale ;
- de quelques conseils pratiques et simples pour éviter les situations "dangereuses".

