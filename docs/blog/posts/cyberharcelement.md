---
title: Cyberharcèlement
date: 2024-07-12
categories:
  - Ressources 
---


# Cyberharcèlement

cyberharcèlement ou harcèlement en ligne ?

Le cyberharcèlement consiste en des agissements en ligne malveillants commis de façon répétée à l’encontre d’une personne. Que faire en cas de harcèlement en ligne ? Ne pas répondre, en parler à un tiers de confiance, verrouiller vos comptes de réseaux sociaux, déposer plainte…


<!-- more -->

!!! success "Ressources de l'éducation nationale"
    [Définition : Qu'est-ce que le harcèlement ?](https://www.education.gouv.fr/non-au-harcelement/qu-est-ce-que-le-harcelement-325361#edugouv-summary-item-)
    
    [Lutte contre le harcèlement à l'école](https://www.education.gouv.fr/non-au-harcelement) 

[Que faire en cas de cyberharcèlement ou harcèlement en ligne ?](https://www.cybermalveillance.gouv.fr/tous-nos-contenus/fiches-reflexes/que-faire-en-cas-de-cyberharcelement-ou-harcelement-en-ligne)


[La fiche réflexe](https://www.cybermalveillance.gouv.fr/medias/2022/09/230422_FicheReflexe_Cyberharcelement.pdf)


[Cyberviolences et cyberharcèlement : que faire ?](https://www.cnil.fr/fr/cyberviolences-et-cyberharcelement-que-faire#:~:text=Selon%20le%20site%20cybermalveillance.gouv,photos%20ou%20vid%C3%A9os%20compromettantes%2C%20etc)

[Les ressources pour prévenir les situations de harcèlement en ligne](https://www.cnil.fr/fr/les-ressources-pour-prevenir-les-situations-de-harcelement-en-ligne)

[L’Association e-Enfance / 3018](https://e-enfance.org/informer/cyber-harcelement/)

