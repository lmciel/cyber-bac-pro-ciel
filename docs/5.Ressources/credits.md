---
hide:
  - toc
author: à compléter
title: Crédits
---


<p>Ce site n'aurait jamais pu voir le jour sans les personnes à l'origine de ces différentes ressources :
<ul>
<li>Charles Poulmaire (académie de Versailles) ;</li>
<li>Pascal Remy (académie de Versailles);</li>
<li>Marius Monnier (académie de Grenoble).</li>
</ul>
<br>
<br>
<div style="text-align: center;"> 
<img src = "../aeif.png" alt='image'>
</div> 
<br>
Il n'aurait pas pu aussi voir le jour sans le soutien indéfectible de l'<a href='https://aeif.fr/' target="_blank"><span style='font-style:italic;'>Association des Enseignantes et Enseignants d'Informatique de France (AEIF)</span></a>.
<br>
<br>
Le site Cybersécurité est hébergé par la forge des communs numériques éducatifs 
<a href='https://docs.forge.apps.education.fr/' target="_blank" style='font-style:italic;'>la forge des communs numériques éducatifs</a>.


<br>
<div style="text-align: center;"> 
<img src = "../brigit_et_komit_transparent.png" alt='image'>
</div> 
<div style="text-align: center;"> 
<img src = "../bandeau_forge.svg" alt='image'>
</div> 
<br>

Elle regroupe les enseignants, communautés d'enseignants et leurs partenaires qui créent et partagent des logiciels et ressources éducatives libres.
Elle rassemble à la fois les professeurs développeurs qui y déposent le code de leurs projets (tels que PrimTux, MathALÉA, e-comBox ou La Nuit du Code) mais aussi les professeurs qui choisissent le format texte ou texte formaté (Markdown, LaTeX, etc.) pour éditer et publier du contenu pédagogique.