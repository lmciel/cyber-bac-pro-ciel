---
hide:
  - toc
author: à compléter
title: Bandes dessinées
---

#  Bandes dessinées

## Les BD de sensibilisation produites par la DSI

Les documents proposés sont composés :

- d’une bande dessinée qui reflète une situation concrète et implique un enseignant ou un acteur de l’Éducation Nationale ;
- de quelques conseils pratiques et simples pour éviter les situations "dangereuses".

<a href="https://edu-html.ac-versailles.fr/phishing4/1/index.html" target="_blank">Spam, phishing et virus : je réfléchis avant de cliquer</a>.

<a href="https://edu-html.ac-versailles.fr/phishing4/2/index.html" target="_blank">Travail à distance : conserver une hygiène numérique pour préserver sa cybersanté</a>.

<a href="https://edu-html.ac-versailles.fr/phishing4/3/index.html" target="_blank">Mot de passe : le mot à ne pas faire passer</a>.


<a href="https://edu-html.ac-versailles.fr/phishing4/" target="_blank">Spam, phishing et virus : un petit clic peut conduire à un grand couac</a>.



![](../chien_mdp3.png){: .center }


A consulter pour aller plus loin.
 Les <a href="https://cybersecurite.forge.apps.education.fr/cyber/1.Bonnes_pratiques/bases/" target="_blank">10 mesures essentielles</a> pour assurer votre cybersécurité.





