---
hide:
  - toc
author: à compléter
title: Romans
---

# Romans

Ces romans offrent une variété de perspectives sur les défis et les dangers liés à la cybersécurité, souvent mêlant suspense, action et réflexion éthique.
Chaque roman offre une perspective unique sur différents aspects de la cybersécurité, faisant d’eux des outils pédagogiques précieux pour comprendre les défis et les implications de la sécurité informatique dans notre monde moderne.
Ils offrent des leçons précieuses sur les dangers et les défis de la cybersécurité, et nous encouragent à adopter de bonnes pratiques pour protéger nos données personnelles et professionnelles.

##  Soldat de la cyberguerre (2024)

Le cyberespace est devenu un lieu d’affrontement majeur. Pour la première fois, voici racontée la création de la cyberdéfense française par son principal fondateur, le vice-amiral d’escadre Arnaud Coustillière.

par Arnaud Coustillière, Aude Leroy (journaliste) aux aux <a href="https://www.tallandier.com/livre/soldat-de-la-cyberguerre/" target="_blank">Éditions Tallandier</a>.


## Cyberattaques : Les dessous d'une menace (2022)

 Hyperconnecté, chacun d’entre nous est une proie des pirates du numérique, capables d’entrer sur nos comptes, de voler nos données personnelles, de bloquer entreprises et gouvernements. Au-delà de simples individus, c’est un réseau complexe de cyberattaquants qui s’est développé à grande échelle. 

par Gerome Billois et Nicolas Cougot, aux <a href="https://www.hachette.fr/livre/cyberattaques-9782019462499" target="_blank">éditions Hachette Livre</a>.


## Les recruteurs (2022) 

Un roman haletant, qui se lit comme un thriller, sur la manifestation des consciences sur les réseaux.
S’insinuer dans les replis de la conscience, trafiquer les émotions, manipuler les rêves. Dans notre monde gouverné par les réseaux et les profils numériques, l’art de la persuasion ne connaît ni frontière, ni morale, ni religion. Un seul résultat compte : trouver une cible, pénétrer son esprit.

par guillaume dasquié, aux <a href="https://www.grasset.fr/livre/les-recruteurs-9782246819394/" target="_blank">éditions Grasset </a>.


## RePlay (2022)

Tristan est un concepteur de jeux vidéo. Il vient de créer RePlay, un programme révolutionnaire qui permet de revivre son passé et defaire d’autres choix. Il l’envoie à quelques milliers de fan pour le tester, et surtout l’adresse à Lois, sa muse, qui refuse pourtant d’avoir des contacts avec lui. Malgré tout, celle-ci se laisse séduire par les possibilités qu’offre le jeu, jusqu’au moment où elle réalise qu’elle est complètement sous son emprise : l'intelligence artificielle prend le contrôle de la vie des joueurs. 

À partir d’un drame intime, et d’une héroïne à laquelle nous pouvons tous nous identifier, Elena Sender réalise un suspense psychologique sidérant sur notre société hyper connectée, où l’intelligence artificielle et la réalité virtuelle dictent nos vies, peut-être même jusqu’à notre propre mort.

 par Elena Sender, aux <a href="https://www.albin-michel.fr/replay-9782226469731" target="_blank">éditions Albin Michel </a>.

Elena Sender est journaliste scientifique à Sciences et avenir et a signé plusieurs films documentaires (Canal Plus, Arte). Elle est l’auteur de deux romans très remarqués : Intrusion (Prix Carrefour du premier roman 2010) et Le Sang des dauphins noirs (2012). Surtout ne mens pas est son troisième roman.


## Cyber Crimes : Un flic 2.0 raconte (2020)

La cybercriminalité explose.
Piratages de données, escroqueries, réseaux pédophiles... Nous sommes tous concernés, souvent sans le savoir.
Ce sont en effet 978 millions de personnes dans le monde qui subissent une cyberattaque chaque année.
Le commandant Pierre Penalba, à la tête du premier groupe de lutte contre la cybercriminalité de la Police nationale, traque quotidiennement les criminels du Net. Dans ce voyage au cœur d’un monde inconnu et pourtant tout proche, il nous dévoile, de l’intérieur, leurs ruses, leurs procédés et le cynisme impressionnant qui guide leurs méfaits.

par Pierre PENALBA, Abigaelle Penalba, aux <a href="https://www.prod.albin-michel-site.infrawan.net/cyber-crimes-9782226445957" target="_blank">éditions Albin Michel</a>.


Le commandant Pierre Penalba est à la tête du premier groupe de lutte contre la cybercriminalité de la Police Nationale. Spécialiste du Net, il est également expert informatique et formateur auprès d’unités de police françaises et étrangères. Il a écrit ce livre avec Abigaelle Penalba.

 Abigaelle Penalba est enseignante et formatrice. Ils ont coécrit aussi 
  Darknet, le voyage qui fait peur.

Qu’est-ce que le Darknet ? Suscitant tous les fantasmes, le Darknet est devenu une légende urbaine. Pour démêler le vrai du faux, les auteurs ont voulu s’y plonger. Ils ont découvert ce que recèle vraiment ce monde occulte.